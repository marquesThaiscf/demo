package com.avaliacao.demo.dao;

import java.util.List;

import com.avaliacao.demo.model.Pessoa;


public interface PessoaDAO {

	public void adicionarPessoa(Pessoa pessoa);
    
    public Pessoa obterPessoaPorId(long id);
    
    public List<Pessoa> listarPessoas();
    
    public void atualizarPessoa(Pessoa pessoa);
    
    public void removerPessoa(long id);
	
}
