package com.avaliacao.demo.dao.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.avaliacao.demo.dao.PessoaDAO;
import com.avaliacao.demo.model.Pessoa;


@Service
@Transactional(propagation = Propagation.REQUIRED)
@Repository
public class PessoaDAOImpl implements PessoaDAO {
	
	@PersistenceContext
    private EntityManager entityManager;
	
	
	@Override
	public void adicionarPessoa(Pessoa pessoa) {
		entityManager.persist(pessoa);
	}
    
	@Override
    public Pessoa obterPessoaPorId(long id) {
		return entityManager.find(Pessoa.class,id);
	}
    
	@Override
    public List<Pessoa> listarPessoas(){
		//return entityManager.createQuery("select ID,NOME,CPF,IDADE from PESSOA").getResultList();
		return entityManager.createQuery("select ID from PESSOA").getResultList();
	}
    
	@Override
    public void atualizarPessoa(Pessoa pessoa) {
		entityManager.merge(pessoa);
	}
    
	@Override
    public void removerPessoa(long id) {
		Pessoa p = entityManager.find(Pessoa.class,id);
        entityManager.remove(p);
	}

}
