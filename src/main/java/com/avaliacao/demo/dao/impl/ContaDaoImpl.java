package com.avaliacao.demo.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.avaliacao.demo.dao.ContaDAO;
import com.avaliacao.demo.model.Conta;


@Service
@Transactional(propagation = Propagation.REQUIRED)
@Repository
public class ContaDaoImpl implements ContaDAO {
	
	@PersistenceContext
    private EntityManager entityManager;
	
	
	@Override
	public void criarConta(Conta conta) {
		entityManager.persist(conta);
	}
	
	@Override
    public Conta obterContaPorId(long id) {
		return entityManager.find(Conta.class,id);
    }
    
	@Override
    public List<Conta> listarContas(){
		return entityManager.createQuery("select ID from CONTA").getResultList();
    }
    
	@Override
    public List<Conta> listarContasPorPessoa(long idTitular){
		return entityManager.createQuery("select ID from CONTA where ID_PESSOA="+Long.toString(idTitular)).getResultList();
    }
    
	@Override
    public double somaSaldo(double creditoEmConta, Conta conta) {
		conta.setSaldo(conta.getSaldo()+creditoEmConta);
		entityManager.merge(conta);
		return (conta.getSaldo()+creditoEmConta);
    }
    
	@Override
    public double subtraiSaldo(double debitoEmConta, Conta conta) {
		conta.setSaldo(conta.getSaldo()-debitoEmConta);
		entityManager.merge(conta);
		return (conta.getSaldo());
    }
}
