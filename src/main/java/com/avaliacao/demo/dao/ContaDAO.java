package com.avaliacao.demo.dao;

import java.util.List;

import com.avaliacao.demo.model.Conta;


public interface ContaDAO {

	public void criarConta(Conta conta);    
    public Conta obterContaPorId(long id);
    public List<Conta> listarContas();    
    public List<Conta> listarContasPorPessoa(long idTitular);
    public double somaSaldo(double creditoEmConta, Conta conta);
    public double subtraiSaldo(double debitoEmConta, Conta conta);

}