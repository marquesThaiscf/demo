package com.avaliacao.demo.jms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.avaliacao.demo.dao.impl.ContaDaoImpl;
import com.avaliacao.demo.model.Conta;


@Component
public class ContaConsumer {
	
	private ContaDaoImpl contaDaoImpl;
    
    @Autowired
    public void Receptador(ContaDaoImpl contaDaoImpl) {
        this.contaDaoImpl = contaDaoImpl;
    }
    
    @JmsListener(destination = "queue.tmDemoTest", containerFactory = "tmDemoFactory")
    public void RecebeMensagem(Conta conta) {
    	contaDaoImpl.criarConta(conta);
    }

}
