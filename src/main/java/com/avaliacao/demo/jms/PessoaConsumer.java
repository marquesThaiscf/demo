package com.avaliacao.demo.jms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.avaliacao.demo.dao.impl.PessoaDAOImpl;
import com.avaliacao.demo.model.Pessoa;

@Component
public class PessoaConsumer {
    
    private PessoaDAOImpl pessoaDAOImpl;
    
    @Autowired
    public void Receptador(PessoaDAOImpl pessoaDAOImpl) {
        this.pessoaDAOImpl = pessoaDAOImpl;
    }
    
    @JmsListener(destination = "queue.tmDemoTest", containerFactory = "tmDemoFactory")
    public void RecebeMensagem(Pessoa pessoa) {
    	pessoaDAOImpl.adicionarPessoa(pessoa);
    }
    
}
