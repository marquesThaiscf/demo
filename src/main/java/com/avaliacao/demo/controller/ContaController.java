package com.avaliacao.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.avaliacao.demo.model.Conta;
import com.avaliacao.demo.dao.impl.ContaDaoImpl;


@RestController
public class ContaController {
		
	@Autowired
    private ContaDaoImpl contaDaoImpl;
	
	
	@RequestMapping(value="/conta", method=RequestMethod.POST, produces="application/json", consumes="application/json")
    public void criarConta(@RequestBody Conta conta){
		contaDaoImpl.criarConta(conta);
    }
	
	@RequestMapping(value="/conta/{id}", produces="application/json", method=RequestMethod.GET)
    public Conta obterContaPorId(@PathVariable("id") long id){
		Conta conta = contaDaoImpl.obterContaPorId(id);
        return conta;
    }
    
	@RequestMapping(value="/contas", produces="application/json", method=RequestMethod.GET)
    public List<Conta> listarContas(){
    	List<Conta> listaContas = contaDaoImpl.listarContas();
        return listaContas;
    }
    
	@RequestMapping(value="/contas/{id}", produces="application/json", method=RequestMethod.GET)
    public List<Conta> listarContasPorPessoa(@PathVariable("id") long id){
    	List<Conta> listaContasPorPessoa = contaDaoImpl.listarContasPorPessoa(id);
        return listaContasPorPessoa;
    }
	
	@RequestMapping(value="/conta/{id}/creditar/{valor}", method=RequestMethod.PUT, produces="application/json", consumes="application/json")
    public void somaSaldo(@PathVariable("id") long id , @PathVariable("valor") double valor){
		Conta conta = contaDaoImpl.obterContaPorId(id);
		contaDaoImpl.somaSaldo(valor, conta);
    }
	
	@RequestMapping(value="/conta/{id}/debitar/{valor}", method=RequestMethod.PUT, produces="application/json", consumes="application/json")
    public void subtraiSaldo(@PathVariable("id") long id , @PathVariable("valor") double valor){
		Conta conta = contaDaoImpl.obterContaPorId(id);
		contaDaoImpl.subtraiSaldo(valor, conta);
    }

}
