package com.avaliacao.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.avaliacao.demo.dao.impl.PessoaDAOImpl;
import com.avaliacao.demo.model.Pessoa;

@RestController
public class PessoaController {
	
	@Autowired
    private PessoaDAOImpl pessoaDAOImpl;
    

    @RequestMapping(value="/pessoa", method=RequestMethod.POST, produces="application/json", consumes="application/json")
    public void adicionarPessoa(@RequestBody Pessoa pessoa){
    	pessoaDAOImpl.adicionarPessoa(pessoa);
    }    

    @RequestMapping(value="/pessoa/{id}",produces="application/json", method=RequestMethod.GET)
    public Pessoa obterPessoaPorId(@PathVariable("id") long id){
    	Pessoa pessoa = pessoaDAOImpl.obterPessoaPorId(id);
        return pessoa;
    }
     
    @RequestMapping(value="/pessoas",produces="application/json", method=RequestMethod.GET)
    public List<Pessoa> listarPessoas(){
    	List<Pessoa> listaPessoas = pessoaDAOImpl.listarPessoas();
        return listaPessoas;
    }    

    @RequestMapping(value="/atualizar", method=RequestMethod.PUT, produces="application/json", consumes="application/json")
    public void atualizarPessoa(@RequestBody Pessoa pessoa){
    	pessoaDAOImpl.atualizarPessoa(pessoa);
    }
    
    @RequestMapping(value="/remover/{id}",method = RequestMethod.DELETE, produces="application/json")
    public void removerPessoa(@PathVariable("id") long id){
    	pessoaDAOImpl.removerPessoa(id);
    }
	
}
