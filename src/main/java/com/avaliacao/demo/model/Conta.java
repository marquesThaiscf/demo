package com.avaliacao.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "CONTA")
public class Conta {
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@ManyToOne
	@JoinColumn(name = "fk_id_pessoa")
	private long idTitular;
	
	@Column(name="saldo")
	private double saldo;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public long getIdPessoaTitular() {
		return idTitular;
	}

	public void setIdPessoaTitular(long idTitular) {
		this.idTitular = idTitular;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	
	@Override
    public String toString() {
        return "Dados da Conta : [IdConta = " + this.id + ", NomeTitular = " + this.idTitular + ", Saldo = " + this.saldo + "]";
    }

}
